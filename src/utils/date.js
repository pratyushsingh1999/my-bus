let getTodayDateString = () => {
    let dtToday = new Date();
    let month = dtToday.getMonth() + 1;
    let day = dtToday.getDate();
    let year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();

    let today = year + '-' + month + '-' + day;
    return today;
}
let getDifferenceBetweenTime = (timeFirst , timeSecond) => {
    let timeFirstArr = timeFirst.split(':');
    let timeSecondArr = timeSecond.split(':');
    let hour = parseInt(timeFirstArr[0]) - parseInt(timeSecondArr[0]);
    let minute = parseInt(timeFirstArr[1]) - parseInt(timeSecondArr[1]);

    if (hour<0) {
        hour = hour*-1;
    }
    if (minute<0) {
        minute = minute*-1;
    }
    const diffTime = hour + "h:" + minute + "m";
    return diffTime;
}

module.exports = {
    getTodayDateString,
    getDifferenceBetweenTime
}
