import React from "react";
import {connect} from "react-redux";
import styles from "./SearchBusesComponent.module.css";
import date from "../../utils/date";
import axios from "axios";
import {updateBus} from "../../store/actions/selectedBusAction";

class SearchBusesComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            busesList: []
        }
    }
    componentDidMount() {
        this.setState({
            loading: true
        })
        axios.post('search/searchBus', {
            sourceId: this.props.sourceCity,
            destinationId: this.props.destinationCity,
            date: this.props.searchDate
        }).then((res) => {
            this.setState({
                loading: false
            })
            this.setState({
                busesList: res.data.routeDetails
            })
        }).catch((err) => {
            console.log(err);
            this.setState({
                loading: false
            })
        })
    }
    bookBusClicked = (tripId) => {
        const trip = this.state.busesList.filter((bus) => bus.id === tripId);
        this.props.updateBus(trip[0]);
        this.props.history.push('bookTicket');
    }
    render() {
        let busesLength = this.state.busesList.length;
        return (
            <React.Fragment>
                {this.state.loading ?
                    <div className="loader" style={{
                        color: "#045676",
                        margin: '60px auto',
                        fontSize: '50px',
                        alignSelf: 'flex-end'
                    }}></div> :
                    <div className={styles.listWrapper}>
                        <p>{busesLength} Buses Found</p>
                        {this.state.busesList.map((bus) => {
                            return (
                                <div className={styles.listItemWrapper} key={bus.id}>
                                    <div className={styles.flexWrapper}>
                                        <div>
                                            <p>{bus.departureTime + " - " + bus.arrivalTime}</p>
                                        </div>
                                        <div className={styles.priceWrapper}>
                                            <span>&#8377; {bus.fare}</span>
                                        </div>
                                    </div>
                                    <div className={styles.flexWrapper}>
                                        <div>
                                            <span className={styles.journeyTime}>{date.getDifferenceBetweenTime(bus.departureTime,bus.arrivalTime)}</span>
                                            <span>  30 Seats</span>
                                        </div>
                                    </div>
                                    <div className={styles.flexWrapper}>
                                        <p>{bus.operatorName}</p>
                                        <button className={styles.bookButton + " waves-effect"}
                                                onClick={() => this.bookBusClicked(bus.id)}>Book Now</button>
                                    </div>
                                    <div className={styles.flexWrapperAmenities}>
                                        <React.Fragment>
                                            {bus.amenities.ac ?
                                                <div className={styles.amenitiesWrapper}><i className="far fa-snowflake"></i> AC</div>
                                                :
                                                null
                                            }
                                            {bus.amenities.chargingPoint ?
                                                <div className={styles.amenitiesWrapper}><i className="fas fa-charging-station"></i> Charging Point</div>
                                                :
                                                null
                                            }
                                            {bus.amenities.liveTracking ?
                                                <div className={styles.amenitiesWrapper}><i className="fas fa-map-marked-alt"></i> Live Tracking</div>
                                                :
                                                null
                                            }
                                            {bus.amenities.wifi ?
                                                <div className={styles.amenitiesWrapper}><i className="fas fa-wifi"></i> Wifi</div>
                                                :
                                                null
                                            }
                                            {bus.amenities.waterBottle ?
                                                <div className={styles.amenitiesWrapper}><i className="fas fa-tint"></i> Water Bottle</div>
                                                :
                                                null
                                            }
                                            {bus.amenities.reschedulable ?
                                                <div className={styles.amenitiesWrapper}><i className="far fa-clock"></i> Reschedulable</div>
                                                :
                                                null
                                            }
                                        </React.Fragment>
                                    </div>
                                </div>

                            )
                        })}
                    </div>
                }
            </React.Fragment>
        )
    }
}
const mapStateToProps = state => ({
    sourceCity: state.search.sourceCity,
    destinationCity: state.search.destinationCity,
    searchDate: state.search.searchDate
})
export default connect(mapStateToProps, {updateBus})(SearchBusesComponent);

