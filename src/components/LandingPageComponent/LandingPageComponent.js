import React from "react";
import vector from "../../assets/bus_vector.png";
import styles from "./LandingPageComponent.module.css";
import {Button, TextField} from "@material-ui/core";
import axios from "axios";
import Autocomplete from '@material-ui/lab/Autocomplete';
import {connect} from "react-redux";
import {updateSearch} from "../../store/actions/searchBusAction";

class LandingPageComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sourceCity: '',
            destinationCity: '',
            dateOfSearch: '',
            allCities: []
        }
    }

    componentDidMount() {
        axios.get('search/allCities')
            .then((res) => {
                this.setState({
                    allCities: res.data
                })
            }).catch((err) => {
            console.log(err);
        })
    }

    onSourceCityChange = (tag, values) => {
        this.setState({
            sourceCity: values.id
        })
    }
    onDestinationCityChange = (tag, values) => {
        this.setState({
            destinationCity: values.id
        })
    }
    searchDateChanged = (event) => {
        event.preventDefault();
        this.setState({
            dateOfSearch: event.target.value
        })
    }
    onSubmit = () => {
        const payload = {
            sourceCity: this.state.sourceCity,
            destinationCity: this.state.destinationCity,
            searchDate: this.state.dateOfSearch
        }
        this.props.updateSearch(payload);
        this.props.history.push('/search');
    }
    render() {
        const wrapperStyle = {
            backgroundImage: `url(${vector})`,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center top 30px',
            backgroundSize: '400px',
            width: '100%',
            height: 'calc(100vh - 84px)'
        }
        return (
            <React.Fragment>
                <div style={wrapperStyle}>
                    <form className={styles.form} onSubmit={this.onSubmit} autoComplete="off">
                        <div className={styles.inputsWrapper}>
                            <Autocomplete
                                options={this.state.allCities}
                                onChange={this.onSourceCityChange}
                                getOptionLabel={(option) => option.name}
                                className={styles.autoComplete}
                                renderInput={(params) => <TextField {...params} label="Enter Source City" variant="outlined"/>}
                            />
                            <Autocomplete
                                options={this.state.allCities}
                                onChange={this.onDestinationCityChange}
                                getOptionLabel={(option) => option.name}
                                className={styles.autoComplete}
                                renderInput={(params) => <TextField {...params} label="Enter Destination City" variant="outlined" />}
                            />
                            <TextField variant="outlined"
                                       name="date"
                                       type="date"
                                       label="Search Date"
                                       InputLabelProps={{
                                           shrink: true,
                                       }}
                                       className={styles.datepicker}
                                       value={this.state.dateOfSearch}
                                       onChange={this.searchDateChanged}/>
                        </div>
                        <Button className={styles.searchButton} variant="contained" type="submit">Search Buses</Button>
                    </form>
                </div>
            </React.Fragment>
        )
    }
}

export default connect(null, {updateSearch})(LandingPageComponent);
