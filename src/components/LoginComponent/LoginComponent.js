import React from "react";
import styles from "./LoginComponent.module.css"
import {Link, Redirect} from "react-router-dom";
import axios from "axios";
import {connect} from "react-redux";
import {loginUser} from "../../store/actions/authAction";
import {updateUser} from "../../store/actions/userAction";
import {TextField} from "@material-ui/core";

class LoginComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            email: '',
            password: '',
            emailError: '',
            passwordError: '',
            responseError: '',
            isFormValid: true
        }
    }

    formInputChange = (event) => {
        const value = event.target.value
        switch (event.target.name) {
            case 'email':
                if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value)) {
                    this.setState({
                        email: value,
                        emailError: ''
                    }, () => {
                        this.checkValid();
                    })
                } else {
                    this.setState({
                        email: value,
                        emailError: 'Email is required and should be a valid email'
                    }, () => {
                        this.checkValid()
                    })
                }
                break;
            case 'password':
                if (value.length < 4) {
                    this.setState({
                        password: value,
                        passwordError: 'Password is required and should be at least 4 character long'
                    }, () => {
                        this.checkValid()
                    })
                } else {
                    this.setState({
                        password: value,
                        passwordError: ''
                    }, () => {
                        this.checkValid();
                    })
                }
                break;
            default:
                break;
        }
    }
    checkValid = () => {
        if (this.state.emailError === '' && this.state.passwordError === '') {
            this.setState({
                isFormValid: true
            })
        } else {
            this.setState({
                isFormValid: false
            })
        }
    }
    onSubmit = (event) => {
        event.preventDefault();
        const user = {
            email: this.state.email,
            password: this.state.password
        }
        this.setState({
            loading: true
        })
        axios.post('auth/login', user)
            .then((res) => {
                this.setState({
                    loading: false,
                    redirect: true
                })
                this.props.loginUser();
                this.props.updateUser(res.data.user);
                localStorage.setItem('auth_token' , res.data.token);
            }).catch((err) => {
                console.log(err);
                this.setState({
                    loading: false,
                    responseError: err.response.data.message
                })
            })
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to="/"></Redirect>
        }
        return (
            <React.Fragment>
                <div className={styles.mainWrapper}>
                    <form className={"card " + styles.formWrapper} onSubmit={this.onSubmit}>
                        <div className={styles.avatarPosition}>
                            <i className="fa fa-user" aria-hidden="true"></i>
                        </div>
                        <p className={styles.welcomeBack}>Welcome Back</p>
                        <p className={styles.formHeader}>Login</p>
                        <TextField name="email"
                                   type="email"
                                   label="Enter Email"
                                   onChange={this.formInputChange}
                                   value={this.state.email} className={styles.inputWrapper}/>
                        <div className={styles.authError}>{this.state.emailError}</div>
                        <TextField name="password"
                                   type="password"
                                   label="Enter Password"
                                   onChange={this.formInputChange}
                                   value={this.state.password}/>
                        <div className={styles.authError}>{this.state.passwordError}</div>

                        <p className={styles.authError + " " + styles.resError}>{this.state.responseError}</p>
                        <Link className={styles.redirectSignup} to="/signup">Dont have an account? Signup!</Link>
                        {
                            this.state.loading ? <div className="loader" style={{
                                    color: "#045676",
                                    margin: '20px 20px 10px',
                                    fontSize: '25px',
                                    alignSelf: 'flex-end'
                                }}></div> :
                                <button type="submit" className={styles.submitButton + " waves-effect waves-light btn"}
                                        disabled={!this.state.isFormValid}>Login</button>
                        }
                    </form>
                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    loggedIn: state.auth.loggedIn
})
export default connect(mapStateToProps, {loginUser, updateUser})(LoginComponent);

