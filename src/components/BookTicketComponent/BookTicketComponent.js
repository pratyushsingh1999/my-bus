import React from "react";
import {connect} from "react-redux";
import styles from "./BookTicketComponent.module.css";
import {Button, TextField} from "@material-ui/core";
import update from "immutability-helper";
import Autocomplete from "@material-ui/lab/Autocomplete";
import axios from "axios";

class BookTicketComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedSeats: [],
            seats: [],
            passengerDetails: [],
            boardingPointId: '',
            droppingPointId: '',
            loading: false
        }
    }

    componentDidMount() {
        this.setState({
            seats: this.props.bus.seats.map((seat) => {
                return {
                    seatNo: seat.seatNo,
                    isBooked: seat.isBooked,
                    selected: false
                }
            })
        })
    }

    seatClicked = (seat) => {
        let selectedSeats = this.state.selectedSeats
        if (selectedSeats.includes(seat)) {
            selectedSeats = selectedSeats.filter(ele => {
                if (ele === seat) {
                    return false
                } else {
                    return true
                }
            })
            this.setState({
                selectedSeats: selectedSeats
            })
        } else {
            this.setState(prevState => ({
                selectedSeats: [...prevState.selectedSeats, seat]
            }))
        }
    }

    onPassengerDetailChange = (event) => {
        return (index) => {
            if (event.target.name === 'name') {
                this.setState(update(this.state, {
                    passengerDetails: {
                        [index]: {
                            $set: {
                                ...this.state.passengerDetails[index],
                                name: event.target.value
                            }
                        }
                    }
                }))
            }
            if (event.target.name === 'age') {
                this.setState(update(this.state, {
                    passengerDetails: {
                        [index]: {
                            $set: {
                                ...this.state.passengerDetails[index],
                                age: event.target.value
                            }
                        }
                    }
                }))
            }
        }
    }

    onBoardingPointChanged = (tag, value) => {
        console.log(value);
        this.setState({
            boardingPointId: value.id
        })
    }
    onDroppingPointChanged = (tag, value) => {
        this.setState({
            droppingPointId: value.id
        })
    }

    proceedToPayment = () => {
        this.setState({
            loading: true
        })
        const bodyData = {
            routeId: this.props.bus.id,
            userId: this.props.userId,
            fare: this.props.bus.fare*this.state.selectedSeats.length,
            seats: JSON.stringify(this.state.selectedSeats),
            passengers: JSON.stringify(this.state.passengerDetails)
        }
        console.log(bodyData);
        axios.put('booking/bookSeats' , bodyData).then((res) => {
            this.props.history.push('/')
            this.setState({
                loading: false
            })
        }).catch((err) => {
            console.log(err);
            this.setState({
                loading: false
            })
        })
    }

    render() {
        let allSeats = [];
        let row = [];
        let seatsWrapper = [];
        for (let i = 0; i < this.state.seats.length; i++) {
            seatsWrapper.push(this.state.seats[i]);
            if (seatsWrapper.length === 2) {
                row.push(seatsWrapper);
                seatsWrapper = []
            }
            if (row.length === 2) {
                allSeats.push(row);
                row = [];
            }
            if ((i + 1) === this.state.seats.length) {
                if (seatsWrapper.length === 0) {
                    if (row.length === 0) {
                        continue;
                    } else {
                        allSeats.push(row);
                        continue
                    }
                }
                if (seatsWrapper.length !== 0) {
                    row.push(seatsWrapper);
                }
                if (row.length !== 0) {
                    allSeats.push(row);
                }

            }
        }

        return (
            <React.Fragment>
                <div>
                    <div className={styles.listItemWrapper}>
                        <div className={styles.flexWrapper}>
                            <div>
                                <p>{this.props.bus.departureTime + " - " + this.props.bus.arrivalTime}</p>
                            </div>
                            <div className={styles.priceWrapper}>
                                <span>&#8377; {this.props.bus.fare * this.state.selectedSeats.length}</span>
                            </div>
                        </div>
                        <div className={styles.flexWrapper}>
                            <p>{this.props.bus.operatorName}</p>
                            <p>Total Fare</p>
                        </div>
                        <div className={styles.flexWrapperAmenities}>
                            <React.Fragment>
                                {this.props.bus.amenities.ac ?
                                    <div className={styles.amenitiesWrapper}><i className="far fa-snowflake"></i> AC
                                    </div>
                                    :
                                    null
                                }
                                {this.props.bus.amenities.chargingPoint ?
                                    <div className={styles.amenitiesWrapper}><i
                                        className="fas fa-charging-station"></i> Charging Point</div>
                                    :
                                    null
                                }
                                {this.props.bus.amenities.liveTracking ?
                                    <div className={styles.amenitiesWrapper}><i
                                        className="fas fa-map-marked-alt"></i> Live Tracking</div>
                                    :
                                    null
                                }
                                {this.props.bus.amenities.wifi ?
                                    <div className={styles.amenitiesWrapper}><i className="fas fa-wifi"></i> Wifi</div>
                                    :
                                    null
                                }
                                {this.props.bus.amenities.waterBottle ?
                                    <div className={styles.amenitiesWrapper}><i className="fas fa-tint"></i> Water
                                        Bottle</div>
                                    :
                                    null
                                }
                                {this.props.bus.amenities.reschedulable ?
                                    <div className={styles.amenitiesWrapper}><i
                                        className="far fa-clock"></i> Reschedulable</div>
                                    :
                                    null
                                }
                            </React.Fragment>
                        </div>
                    </div>
                    <div className={styles.busMainWrapper}>
                        <p className={styles.busMainWrapperHeading}>Select Seats</p>
                        <div className={styles.busSeatsWrapper}>
                            <div className={styles.steeringWheel}>
                                <i className="fas fa-dharmachakra"></i>
                            </div>
                            {
                                allSeats.map((row, index) => (
                                    <div className={styles.busRow} key={"row" + (index + 1)}>
                                        {row.map((seatsWrapper, index) => (
                                            <div className={styles.seatsWrapper} key={"seatWrapper" + (index + 1)}>
                                                {seatsWrapper.map((seat) => (
                                                    <div onClick={() => this.seatClicked(seat.seatNo)}
                                                         className={this.state.selectedSeats.includes(seat.seatNo) ? styles.seatSelected : seat.isBooked ? styles.seatBooked : styles.seat}
                                                         key={seat.seatNo}></div>
                                                ))}
                                            </div>
                                        ))}
                                    </div>
                                ))
                            }
                        </div>
                        <div>
                            <div className={styles.seatDescriptionWrapper}>
                                <div className={styles.seatBooked}></div>
                                <span>Booked</span>
                                <div className={styles.seat}></div>
                                <span>Available</span>
                                <div className={styles.seatSelected}></div>
                                <span>Selected</span>
                            </div>
                        </div>
                        {/*Boarding Points Droping Points*/}
                        <div className={styles.boardingPointDroppingPointWrapper}>
                            <Autocomplete
                                options={this.props.bus.boardingPoints}
                                onChange={this.onBoardingPointChanged}
                                getOptionLabel={(option) => option.name}
                                className={styles.autoComplete}
                                renderInput={(params) => <TextField {...params} autoComplete="false" label="Select Boarding Point" variant="outlined"/>}
                            />
                            <Autocomplete
                                options={this.props.bus.droppingPoints}
                                onChange={this.onDroppingPointChanged}
                                getOptionLabel={(option) => option.name}
                                className={styles.autoComplete}
                                renderInput={(params) => <TextField {...params} autoComplete="false" label="Select Dropping Point" variant="outlined" />}
                            />
                        </div>
                        {/*passenger details*/}
                        <div className={styles.passengerDetailsWrapper}>
                            {this.state.selectedSeats.map((seatNumber, index) => (
                                <div>
                                    <p>Enter Passenger {index + 1} details</p>
                                    <div className="input-field">
                                        <input name="name"
                                               onChange={(event) => this.onPassengerDetailChange(event)(index)}
                                               id={seatNumber + "name"} type="text"/>
                                        <label htmlFor={seatNumber + "name"}>Enter Name</label>
                                    </div>
                                    <div className="input-field">
                                        <input name="age"
                                               onChange={(event) => this.onPassengerDetailChange(event)(index)}
                                               id={seatNumber + "age"} type="number"/>
                                        <label htmlFor={seatNumber + "age"}>Enter Age</label>
                                    </div>
                                </div>
                            ))}
                        </div>

                        <div className={styles.proceedToBookWrapper}>
                            {this.state.selectedSeats.length === 0 ?
                                <p>Select Seats</p>
                                :
                                <p>Proceed to book {this.state.selectedSeats.length} seats</p>
                            }
                            {
                                this.state.loading ?
                                    <div className="loader" style={{
                                        color: "#045676",
                                        margin: '20px 20px 10px',
                                        fontSize: '25px',
                                        alignSelf: 'flex-end'
                                    }}></div>
                                    :
                                    <Button className={styles.proceedButton}
                                            onClick={this.proceedToPayment}
                                            color="primary"
                                            variant="contained">
                                        Proceed to Payment
                                    </Button>
                            }
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    bus: state.selectedBus,
    userId: state.user.id
})
export default connect(mapStateToProps, null)(BookTicketComponent);

