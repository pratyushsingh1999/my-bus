import React from "react";
import {connect} from "react-redux";
import styles from "./MyBookingsComponent.module.css";
import axios from "axios";

class MyBookingsComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            upcoming: true,
            completed: false,
            isLoading: false,
            bookingsListUpcoming: [],
            bookingsListCompleted: []
        }
    }

    changeTab = (event) => {
        switch (event.target.name) {
            case 'upcoming':
                this.setState({
                    upcoming: true,
                    completed: false
                })
                break;
            case 'completed':
                this.setState({
                    upcoming: false,
                    completed: true
                })
                break;
            default:
                break
        }
    }

    cancelBooking = (routeId , tripId) => {
        console.log(tripId);
        let userId = this.props.user.id;
        axios.put('booking/cancelBooking' , {
            userId,
            routeId,
            tripId
        }).then((res) => {
            console.log(res);
        }).catch((err) => {
            console.log(err);
        })
        this.setState(prevState => ({
            bookingsListUpcoming: prevState.bookingsListUpcoming.filter((tripId) => {
                return !tripId.tripId === tripId;
            })
        }))
    }

    componentDidMount() {
        this.setState({
            isLoading: true
        })
        axios.post('booking/allBookings', {
            userId: this.props.user.id
        }).then((res) => {
            this.setState({
                bookingsListUpcoming: res.data.upcomingTrips,
                bookingsListCompleted: res.data.completedTrips,
                isLoading: false
            })

            console.log(res);
        }).catch((err) => {
            console.log(err);
            this.setState({
                isLoading: false
            })
        })
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.state.isLoading ?
                        <div className="loader" style={{
                            color: "#045676",
                            margin: '50px auto',
                            fontSize: '50px',
                        }}></div>
                        :
                        <div>
                            <div className={styles.header}>
                                <button onClick={this.changeTab} className={this.state.upcoming ? styles.active : ""}
                                        name="upcoming">Upcoming
                                </button>
                                <button onClick={this.changeTab} className={this.state.completed ? styles.active : ""}
                                        name="completed">Completed
                                </button>
                            </div>
                            <div className={styles.mainWrapper}>
                                {this.state.upcoming ?
                                    <div>
                                        {
                                            this.state.bookingsListUpcoming.length === 0 ?
                                                <p>No Upcoming Bookings</p> :
                                                <div>
                                                    {this.state.bookingsListUpcoming.map((trip) => (
                                                        <div className={styles.tripWrapper + " card"}>
                                                            <div className={styles.flexWrapper}>
                                                                <p>{trip.sourceCity} to {trip.destinationCity}</p>
                                                                <p>&#8377; {trip.fare}</p>
                                                            </div>
                                                            <p className={styles.time}>{trip.departureTime} - {trip.arrivalTime}</p>
                                                            <div className={styles.flexWrapper}>
                                                                <div>
                                                                    <p className={styles.operatorName}>{trip.operator}</p>
                                                                    <p className={styles.date}>{trip.startDate}</p>
                                                                </div>
                                                                <button onClick={() => this.cancelBooking(trip.routeId, trip.tripId)} className={styles.cancelBooking}>Cancel Booking</button>
                                                            </div>
                                                            <table>
                                                                <tbody>
                                                                {trip.passengers.map((passenger) => (
                                                                    <React.Fragment>
                                                                        <tr>
                                                                            <td>Name</td>
                                                                            <td>{passenger.name}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Seat No</td>
                                                                            <td>{passenger.seatNumber}</td>
                                                                        </tr>
                                                                    </React.Fragment>
                                                                ))}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    ))}
                                                </div>
                                        }
                                    </div>
                                    : null
                                }
                                {this.state.completed ?
                                    <div>
                                        {
                                            this.state.bookingsListCompleted.length === 0 ?
                                                <p>No Completed Bookings</p> :
                                                <div>
                                                    {this.state.bookingsListUpcoming.map((trip) => (
                                                        <div className={styles.tripWrapper + " card"}>
                                                            <div className={styles.flexWrapper}>
                                                                <p>{trip.sourceCity} to {trip.destinationCity}</p>
                                                                <p>&#8377; {trip.fare}</p>
                                                            </div>
                                                            <p className={styles.time}>{trip.departureTime} - {trip.arrivalTime}</p>
                                                            <div className={styles.flexWrapper}>
                                                                <div>
                                                                    <p className={styles.operatorName}>{trip.operator}</p>
                                                                    <p className={styles.date}>{trip.startDate}</p>
                                                                </div>
                                                            </div>
                                                            <table>
                                                                <tbody>
                                                                {trip.passengers.map((passenger) => (
                                                                    <React.Fragment>
                                                                        <tr>
                                                                            <td>Name</td>
                                                                            <td>{passenger.name}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Seat No</td>
                                                                            <td>{passenger.seatNumber}</td>
                                                                        </tr>
                                                                    </React.Fragment>
                                                                ))}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    ))}
                                                </div>
                                        }
                                    </div>
                                    : null
                                }
                            </div>
                        </div>
                }
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user,
})
export default connect(mapStateToProps, null)(MyBookingsComponent);

