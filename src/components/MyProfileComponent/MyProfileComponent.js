import React from "react";
import {connect} from "react-redux";
import styles from "./MyProfileComponent.module.css";
import axios from "axios";
import { updateUserSub } from "../../store/actions/userAction";

class MyProfileComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: '',
            lastname: '',
            phoneNumber: '',
            editModeOn: false,
            isSaving: false
        }
    }
    componentDidMount() {
        this.setState({
            firstname: this.props.user.firstname,
            lastname: this.props.user.lastname,
            phoneNumber: this.props.user.phoneNumber || ''
        }, () => console.log(this.state));
    }
    onInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    saveChanges = () => {
        this.setState({
            isSaving: true
        })
        const updatedUser = {
            userId: this.props.user.id,
            firstName: this.state.firstname,
            lastName: this.state.lastname,
            phoneNumber: this.state.phoneNumber
        }
        axios.put('user/updateUser' , updatedUser).then((res) => {
            this.setState({
                isSaving: false,
                editModeOn: false
            })
            this.props.updateUserSub({
                firstname: updatedUser.firstName,
                lastname: updatedUser.lastName,
                phoneNumber: updatedUser.phoneNumber
            })
        }).catch((err) => {
            this.setState({
                isSaving: false,
                editModeOn: false
            })
            console.log(err);
        })
    }
    render() {
        return (
            <React.Fragment>
                <div className={styles.profileWrapper}>
                    {!this.state.editModeOn ?
                        <div className={"card " + styles.card}>
                            <table>
                                <tbody>
                                <tr>
                                    <td>First Name</td>
                                    <td>{this.props.user.firstname}</td>
                                </tr>
                                <tr>
                                    <td>Last Name</td>
                                    <td>{this.props.user.lastname}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{this.props.user.email}</td>
                                </tr>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td>{this.props.user.dob}</td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td>{this.props.user.gender}</td>
                                </tr>
                                <tr>
                                    <td>Phone no</td>
                                    <td>{this.props.user.phoneNumber || 'No Phone Number Added'}</td>
                                </tr>
                                </tbody>
                            </table>
                            <button className={styles.editButton} onClick={() => this.setState({editModeOn: true})}>
                                <i className="fas fa-user-edit"></i>
                                Edit Profile
                            </button>
                        </div>
                        :
                        <div>
                            <div className={"card " + styles.card}>
                                <div className="input-field">
                                    <input id="firstname"
                                           name="firstname"
                                           type="text"
                                           onChange={this.onInputChange}
                                           value={this.state.firstname}/>
                                        <label className="active" htmlFor="firstname">First Name</label>
                                </div>
                                <div className="input-field">
                                    <input id="lastname"
                                           name="lastname"
                                           type="text"
                                           onChange={this.onInputChange}
                                           value={this.state.lastname}/>
                                        <label className="active" htmlFor="lastname">Last Name</label>
                                </div>
                                <div className="input-field">
                                    <input id="phoneNumber"
                                           name="phoneNumber"
                                           type="text"
                                           onChange={this.onInputChange}
                                           value={this.state.phoneNumber}/>
                                        <label className="active" htmlFor="phoneNumber">Phone Number</label>
                                </div>
                                {
                                    this.state.isSaving ?
                                        <div className="loader" style={{
                                            color: "#045676",
                                            margin: '20px 20px 10px',
                                            fontSize: '25px',
                                            alignSelf: 'flex-end'
                                        }}></div>
                                        :
                                        <div className={styles.editButtonWrapper}>
                                            <button className={styles.editButton} onClick={() => this.setState({editModeOn: false})}>
                                                <i className="fas fa-arrow-left"></i>
                                                Go Back
                                            </button>
                                            <button className={styles.editButton} onClick={this.saveChanges}>
                                                <i className="fas fa-user-edit"></i>
                                                Save Changes
                                            </button>
                                        </div>

                                }
                            </div>
                        </div>
                    }
                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user,
})
export default connect(mapStateToProps, {updateUserSub})(MyProfileComponent);

