import React from "react";
import styles from "./NavBarComponent.module.css"
import bus from "../../assets/bus.png"
import materialize from "materialize-css"
import background from "../../assets/bus_seats.jpg"

import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {logoutUser} from "../../store/actions/authAction";

class NavBarComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogged: false
        }
    }
    componentDidMount() {
        let sidenav = document.querySelector('#slide-out');
        materialize.Sidenav.init(sidenav, {});
    }
    logoutUser = (event) => {
        event.preventDefault();
        this.props.logoutUser();
        localStorage.removeItem('auth_token');
        this.props.history.push('/');
    }

    render() {
        return (
            <React.Fragment>
                <div className={styles.wrapper}>
                    <div className={styles.subWrapper}>
                        <img src={bus} alt={"Logo"}/>
                        <p>Welcome to My Bus</p>
                    </div>
                    <div className={styles.subWrapper}>
                        <Link className={styles.navLink} to="/">Home</Link>
                        {!this.props.loggedIn ?
                            <React.Fragment>
                                <Link className={styles.navLink} to="/login">Login</Link>
                                <Link className={styles.navLink} to="/signup">Signup</Link>
                            </React.Fragment> :
                            <React.Fragment>
                                <Link className={styles.navLink} to="/bookings">My Bookings</Link>
                                <Link className={styles.navLink} to="/profile">{this.props.username}</Link>
                                <a href="/" className={styles.navLink} onClick={this.logoutUser}>Logout</a>
                            </React.Fragment>
                        }
                        <div data-target="slide-out" className={"sidenav-trigger " + styles.sideNavToggle}>
                            <i className="material-icons">menu</i>
                        </div>
                        <ul id="slide-out" className="sidenav">
                            {this.props.loggedIn ?
                                <li>
                                    <div className="user-view">
                                        <div className="background">
                                            <img className={styles.sideNavBackground} src={background}
                                                 alt="Background"/>
                                        </div>
                                        <div className={styles.userIconSideNav}>
                                            <i className="fas fa-user-circle"></i>
                                        </div>
                                        <div><span className={"white-text " + styles.sideNavWelcome}>Welcome</span></div>
                                        <div><span className="white-text name">{this.props.username}</span></div>
                                        <div><span className="white-text email">{this.props.email}</span></div>
                                    </div>
                                </li> : ''
                            }
                            <li>
                                <Link className="waves-effect sidenav-close" to="/">
                                    <i className="fas fa-home"></i>
                                    Home
                                </Link>
                            </li>
                            {
                                !this.props.loggedIn ?
                                    <React.Fragment>
                                        <li>
                                            <Link className="waves-effect sidenav-close" to="/login">
                                                <i className="fas fa-sign-in-alt"></i>
                                                Login
                                            </Link>
                                        </li>
                                        <li>
                                            <Link className="waves-effect sidenav-close" to="/signup">
                                                <i className="fas fa-user-plus"></i>
                                                Signup
                                            </Link>
                                        </li>
                                    </React.Fragment> :
                                    <React.Fragment>
                                        <li>
                                            <Link className="waves-effect sidenav-close" to="/bookings">
                                                <i className="fas fa-suitcase-rolling"></i>
                                                My Bookings
                                            </Link>
                                        </li>
                                        <li>
                                            <Link className="waves-effect sidenav-close" to="/profile">
                                                <i className="fas fa-user-cog"></i>
                                                My Profile
                                            </Link>
                                        </li>
                                        <li>
                                            <a href="/" className="waves-effect sidenav-close" onClick={this.logoutUser}>
                                                <i className="fas fa-sign-out-alt"></i>
                                                Logout
                                            </a>
                                        </li>
                                    </React.Fragment>
                            }

                            <li>
                                <div className="divider"></div>
                            </li>
                            <li>
                                <Link className="waves-effect sidenav-close" to="/help">
                                    <i className="fas fa-question"></i>
                                    Help
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    loggedIn: state.auth.loggedIn,
    username: state.user.firstname,
    email: state.user.email
})
export default connect(mapStateToProps, {logoutUser})(NavBarComponent);

