import React from "react";
import styles from "./SignUpComponent.module.css"
import {Link, Redirect} from "react-router-dom";
import date from "../../utils/date"
import axios from "axios";
import {
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    TextField
} from "@material-ui/core";

class SignUpComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            firstname: '',
            lastname: '',
            gender: 'Male',
            dob: '',
            email: '',
            password: '',
            passwordConfirm: '',
            isFormValid: false,
            formErrors: {},
            redirect: false
        }
    }

    componentDidMount() {
    }

    formInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        }, () => {
            this.validateForm(event);
        })
    }
    validateForm = (event) => {
        const formData = {
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            dob: this.state.dob,
            gender: this.state.gender,
            email: this.state.email,
            password: this.state.password,
            confirmPassword: this.state.passwordConfirm
        }
        switch (event.target.name) {
            case 'firstname':
                if (formData.firstname.length < 1) {
                    this.setState((state) => ({
                        formErrors: {
                            ...state.formErrors,
                            firstname: 'First name is required'
                        }
                    }))
                } else {
                    this.setState((state) => ({
                        formErrors: {
                            ...state.formErrors,
                            firstname: ''
                        }
                    }))
                }
                break;
            case 'dob':
                if (formData.dob === '') {
                    this.setState((state) => ({
                        formErrors: {
                            ...state.formErrors,
                            dob: 'Date of birth is required'
                        }
                    }))
                } else {
                    this.setState((state) => ({
                        formErrors: {
                            ...state.formErrors,
                            dob: ''
                        }
                    }))
                }
                break;
            case 'email':
                if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(formData.email)) {
                    this.setState((state) => ({
                        formErrors: {
                            ...state.formErrors,
                            email: ''
                        }
                    }))
                } else {
                    this.setState((state) => ({
                        formErrors: {
                            ...state.formErrors,
                            email: 'Email is required and enter a valid email'
                        }
                    }))
                }
                break;
            case 'password':
                if (formData.password.length < 4) {
                    this.setState((state) => ({
                        formErrors: {
                            ...state.formErrors,
                            password: 'Password is required and should be minimum 4 characters'
                        }
                    }))
                } else {
                    this.setState((state) => ({
                        formErrors: {
                            ...state.formErrors,
                            password: ''
                        }
                    }))
                }
                break;
            case 'passwordConfirm':
                if (formData.password === formData.confirmPassword) {
                    this.setState((state) => ({
                        formErrors: {
                            ...state.formErrors,
                            passwordConfirm: ''
                        }
                    }))
                } else {
                    this.setState((state) => ({
                        formErrors: {
                            ...state.formErrors,
                            passwordConfirm: 'Password and confirm password do not match'
                        }
                    }))
                }
                break;
            default:
                break;
        }
        this.updateFormValidStatus();
    }
    updateFormValidStatus = () => {
        this.setState({
            isFormValid: false
        }, () => {
            let errors = this.state.formErrors;
            let count = 0;
            Object.values(errors).forEach((error) => {
                if (error === '' || error === null) {
                    count++;
                }
            })
            if (count === 5) {
                this.setState({
                    isFormValid: true
                })
            } else {
                this.setState({
                    isFormValid: false
                })
            }
        })
    }
    onSubmit = (event) => {
        event.preventDefault();
        this.setState({
            loading: true
        })
        const user = {
            firstName: this.state.firstname,
            lastName: this.state.lastname,
            dob: this.state.dob,
            gender: this.state.gender,
            email: this.state.email,
            password: this.state.password
        }
        axios.post('auth/signup', user)
            .then((res) => {
                this.setState({
                    loading: false,
                    redirect: true
                })
            }).catch((err) => {
            this.setState({
                loading: false
            })
            console.log(err)
        })
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to="/login"></Redirect>
        }
        return (
            <React.Fragment>
                <div className={styles.mainWrapper}>
                    <form className={"card " + styles.formWrapper} onSubmit={this.onSubmit}>
                        <div className={styles.avatarPosition}>
                            <i className="fa fa-user-plus" aria-hidden="true"></i>
                        </div>
                        <p className={styles.welcomeBack}>Create Account</p>
                        <p className={styles.formHeader}>Signup</p>

                        <div className={styles.siblingInputsWrapper}>
                            <div className={styles.inputWrapper}>
                                <TextField name="firstname"
                                           type="text"
                                           label="Enter First Name"
                                           value={this.state.firstname}
                                           onChange={this.formInputChange}/>
                                <div className={styles.authError}>{this.state.formErrors.firstname}</div>
                            </div>
                            <div className={styles.inputWrapper}>
                                <TextField name="lastname"
                                           type="text"
                                           label="Enter Last Name"
                                           value={this.state.lastname}
                                           onChange={this.formInputChange}/>
                            </div>
                        </div>
                        <div className={styles.siblingInputsWrapper}>
                            <div className={styles.inputWrapper}>
                                <TextField name="dob"
                                           type="date"
                                           label="Select Date of Birth"
                                           max={date.getTodayDateString()}
                                           InputLabelProps={{
                                               shrink: true,
                                           }}
                                           value={this.state.dob}
                                           onChange={this.formInputChange}/>
                                <div className={styles.authError}>{this.state.formErrors.dob}</div>
                            </div>
                            <div className={styles.inputWrapper}>
                                <FormControl>
                                    <InputLabel id="gender">Gender</InputLabel>
                                    <Select
                                        labelId="gender"
                                        value={this.state.gender}
                                        onChange={this.formInputChange}
                                        name="gender"
                                    >
                                        <MenuItem value="Male">Male</MenuItem>
                                        <MenuItem value="Female">Female</MenuItem>
                                    </Select>
                                </FormControl>
                            </div>
                        </div>
                        <div className={styles.inputWrapper}>
                            <TextField name="email"
                                       type="email"
                                       value={this.state.email}
                                       label="Enter Email"
                                       onChange={this.formInputChange}/>
                            <div className={styles.authError}>{this.state.formErrors.email}</div>
                        </div>
                        <div className={styles.inputWrapper}>
                            <TextField id="standard-basic"
                                       name="password"
                                       type="password"
                                       label="Enter Password"
                                       value={this.state.password}
                                       onChange={this.formInputChange}/>
                            <div className={styles.authError}>{this.state.formErrors.password}</div>
                        </div>
                        <div className={styles.inputWrapper}>
                            <TextField name="passwordConfirm"
                                       type="password"
                                       label="Confirm Password"
                                       value={this.state.passwordConfirm}
                                       onChange={this.formInputChange}/>
                            <div className={styles.authError}>{this.state.formErrors.passwordConfirm}</div>
                        </div>
                        <Link className={styles.redirectSignup} to="/login">Already have an account? Login!</Link>
                        {
                            this.state.loading ? <div className="loader" style={{
                                    color: "#045676",
                                    margin: '20px 20px 10px',
                                    fontSize: '25px',
                                    alignSelf: 'flex-end'
                                }}></div> :
                                <button type="submit" className={styles.submitButton + " waves-effect waves-light btn"}
                                        disabled={!this.state.isFormValid}>Signup</button>
                        }
                    </form>
                </div>
            </React.Fragment>
        )
    }
}

export default SignUpComponent;
