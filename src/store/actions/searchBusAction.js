import {UPDATE_SEARCH} from "../actions/types";

export const updateSearch = (payload) => (dispatch) => {
    dispatch({
        type: UPDATE_SEARCH,
        payload: payload
    });
}

