import {UPDATE_BUS} from "../actions/types";

export const updateBus = (payload) => (dispatch) => {
    dispatch({
        type: UPDATE_BUS,
        payload: payload
    });
}

