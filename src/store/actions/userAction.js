import {UPDATE_USER , UPDATE_USER_SUB} from "./types";

export const updateUser = (payload) => (dispatch) => {
    dispatch({
        type: UPDATE_USER,
        payload: payload
    })
}
export const updateUserSub = (payload) => (dispatch) => {
    dispatch({
        type: UPDATE_USER_SUB,
        payload: payload
    })
}
