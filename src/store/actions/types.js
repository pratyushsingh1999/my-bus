export const LOGIN_USER = 'LOGIN_USER';
export const LOGOUT_USER = 'LOGOUT_USER';

export const UPDATE_USER = 'UPDATE_USER';
export const UPDATE_USER_SUB = 'UPDATE_USER_SUB';

export const UPDATE_SEARCH = 'UPDATE_SEARCH'

export const UPDATE_BUS = 'UPDATE_BUS'
