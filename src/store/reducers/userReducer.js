import { UPDATE_USER , UPDATE_USER_SUB } from "../actions/types";

const initialState = {
    id: '',
    email: '',
    dob: '',
    gender: '',
    firstname: '',
    lastname: '',
    phoneNumber: ''
}

const reducer = (state = initialState , action) => {
    switch (action.type) {
        case UPDATE_USER:
            return {
                ...state,
                id: action.payload.id,
                email: action.payload.emailId,
                firstname: action.payload.firstName,
                lastname: action.payload.lastName,
                gender: action.payload.gender,
                dob: action.payload.dob,
                phoneNumber: action.payload.phoneNumber
            }
        case UPDATE_USER_SUB:
            return {
                ...state,
                firstname: action.payload.firstname,
                lastname: action.payload.lastname,
                phoneNumber: action.payload.phoneNumber
            }
        default: return state;
    }
}

export default reducer;
