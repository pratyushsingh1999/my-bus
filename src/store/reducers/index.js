import { combineReducers } from "redux";
import authReducer from "./authReducer";
import userReducer from "./userReducer";
import searchBusReducer from "./searchBusReducer";
import selectedBusReducer from "./selectedBusReducer";

export default combineReducers({
    auth: authReducer,
    user: userReducer,
    search: searchBusReducer,
    selectedBus: selectedBusReducer
})
