import { LOGIN_USER , LOGOUT_USER} from "../actions/types";

const initialState = {
    loggedIn: false
}

const reducer = (state = initialState , action) => {
    switch (action.type) {
        case LOGIN_USER:
            return {
                ...state,
                loggedIn: true
            }
        case LOGOUT_USER:
            return {
                ...state,
                loggedIn: false
            }
        default: return state;
    }
}

export default reducer;
