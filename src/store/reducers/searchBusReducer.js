import { UPDATE_SEARCH } from "../actions/types";

const initialState = {
    sourceCity: '',
    destinationCity: '',
    searchDate: ''
}

const reducer = (state = initialState , action) => {
    switch (action.type) {
        case UPDATE_SEARCH:
            return {
                ...state,
                sourceCity: action.payload.sourceCity,
                destinationCity: action.payload.destinationCity,
                searchDate: action.payload.searchDate,
            }
        default: return state;
    }
}

export default reducer;
