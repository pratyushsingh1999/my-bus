import {UPDATE_BUS} from "../actions/types";

const initialState = {
    amenities: '',
    arrivalTime: '',
    busId: '',
    departureTime: '',
    endDate: '',
    fare: '',
    id: '',
    operatorId: '',
    operatorName: '',
    startDate: '',
    seats: '',
    boardingPoints: '',
    droppingPoints: ''
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_BUS:
            return {
                ...state,
                amenities: action.payload.amenities,
                arrivalTime: action.payload.arrivalTime,
                busId: action.payload.busId,
                departureTime: action.payload.departureTime,
                endDate: action.payload.endDate,
                fare: action.payload.fare,
                id: action.payload.id,
                operatorId: action.payload.operatorId,
                operatorName: action.payload.operatorName,
                startDate: action.payload.searchDate,
                seats: action.payload.seats,
                boardingPoints: action.payload.boardingPoints,
                droppingPoints: action.payload.droppingPoints
            }
        default:
            return state;
    }
}

export default reducer;
