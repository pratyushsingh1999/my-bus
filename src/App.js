import React from "react";
import './App.css';
import LoginComponent from "./components/LoginComponent/LoginComponent";
import SignUpComponent from "./components/SignUpComponent/SignUpComponent";
import LandingPageComponent from "./components/LandingPageComponent/LandingPageComponent";
import {loginUser, logoutUser} from "./store/actions/authAction";
import {updateUser} from "./store/actions/userAction"

import {
    BrowserRouter as Router,
    Switch,
    Route, Redirect,
} from "react-router-dom";

import NavBarComponent from "./components/NavBarComponent/NavBarComponent";
import {connect} from "react-redux";

import axios from "axios";
import MyBookingsComponent from "./components/MyBookingsComponent/MyBookingsComponent";
import MyProfileComponent from "./components/MyProfileComponent/MyProfileComponent";
import SearchBusesComponent from "./components/SearchBusesComponent/SearchBusesComponent";
import BookTicketComponent from "./components/BookTicketComponent/BookTicketComponent";

axios.defaults.baseURL = "https://backend-redbus-mrinal.herokuapp.com/";
axios.defaults.headers = {
    authorization: 'bearer ' + localStorage.getItem("auth_token"),
    'Content-Type': 'application/json;charset=UTF-8',
    "Access-Control-Allow-Origin": "*",
    "Accept": "application/json"
}

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }

    componentDidMount() {
        const token = localStorage.getItem('auth_token');
        if (token === undefined || token === null) {
            this.props.logoutUser();
            this.setState({
                loading: false
            })
        } else {
            axios.get('user/getUserByToken')
                .then((res) => {
                    this.setState({
                        loading: false
                    })
                    this.props.loginUser();
                    this.props.updateUser(res.data.user);
                })
                .catch((err) => {
                    this.props.logoutUser();
                    this.setState({
                        loading: false
                    })
                })
        }
    }

    render() {
        return (
            <div>
                {!this.state.loading ?
                    <Router>
                        <Route path="/" component={NavBarComponent}></Route>
                        <Switch>
                            <Route exact path="/" component={LandingPageComponent}></Route>
                            <Route exact path="/login" component={LoginComponent}></Route>
                            <Route exact path="/signup" component={SignUpComponent}></Route>
                            <Route exact path="/search" component={SearchBusesComponent}></Route>
                            {
                                this.props.loggedIn ?
                                    <React.Fragment>
                                        <Route exact path="/bookings" component={MyBookingsComponent}></Route>
                                        <Route exact path="/profile" component={MyProfileComponent}></Route>
                                        <Route exact path="/bookTicket" component={BookTicketComponent}></Route>
                                    </React.Fragment> :
                                    <React.Fragment>
                                    </React.Fragment>
                            }
                            <Route><Redirect to="/"></Redirect></Route>
                        </Switch>
                    </Router> :
                    <div className="loader" style={{
                        color: "#045676",
                        margin: '60px auto',
                        fontSize: '50px',
                        alignSelf: 'flex-end'
                    }}></div>
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    loggedIn: state.auth.loggedIn
})
export default connect(mapStateToProps, {loginUser, logoutUser, updateUser})(App);
